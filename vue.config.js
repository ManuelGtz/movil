module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  publicPath: '',

  pwa: {
    themeColor: '#4A90E2',
    msTileColor: '#4A90E2',
    manifestOptions: {
      background_color: '#4A90E2'
    }
  },

  pluginOptions: {
    cordovaPath: 'src-cordova'
  }
}
